var sayHello = "Hello World";
console.log(sayHello);

//VARIABLE & DATA TYPE
var name = "John"; //string
var angka = 12; //number
var todayaIsFriday = false; //boolean
console.log(name);
console.log(angka);
console.log(todayaIsFriday);

var items;
console.log(items); //undefined

//STRING PROPERTIES
//.length
var word = "Javascript is awesome";
console.log(word.length);

//STRING METHODS
//.charAt([indeks])
console.log('I am a string'.charAt(3)); // outuput: m

//.concat([string])
var string1 = "good";
var string2 = 'luck';
console.log(string1.concat(string2)); //goodluck
console.log(string2.concat(string1)); //luckgood

//.indexOf([string/karakter])
var text = 'dung dung ces1';
console.log(text.indexOf('dung')); // array ke 0
console.log(text.indexOf('u')); // array ke 1
console.log(text.indexOf('jreng')); //-1 karena tidak ditemukan

//.substring([indeks awal], [indeks akhir (optional)])
var car1 = 'Lykan Hypersport';
var car2 = car1.substr(6); //mulai dari index ke 6, huruf H
console.log(car2); // output: Hypersport

//.substr([indeks awal], [jumlah karakter yang diambil (optional)])
var motor1 = 'zelda motor';
var motor2 = motor1.substr(2,2); //ambil index ke 2 sebanyak 2 karakter
console.log(motor2);

//.toUpperCase()
var letter = 'This letter is For you';
var upper = letter.toUpperCase();
console.log(upper); //out: this letter is for you

//.toLowerCase()
var letter = 'This letter is For you';
var lower = letter.toLowerCase();
console.log(lower); //out: this letter is for you

//.trim()
var username = ' Sa ya Administra tor ';
var newUsername = username.trim();
console.log(newUsername); //menghilangkan spasi di awal dan akhir text username

//String([angka/array])
//Fungsi global String() dapat dipanggil kapan saja pada program JavaScript dan akan mengembalikan data dalam tipe data String dari parameter yang diberikan.
var int = 12;
var real = 3.45;
var arr = [6, 7, 8];

var strInt = String(int);
var strReal = String(real);
var strArr = String(arr);

console.log(strInt);
console.log(strReal);
console.log(strArr);

//.toString()
//Mengonversi tipe data lain menjadi string. Bila data tersebut adalah array, setiap nilai akan dituliskan dan dipisah dengan karakter koma.
var number = 21;
console.log(number.toString()); //'21'
var array = [1,2];
console.log(array.toString()); //'1,2'

//Number([String])
//Fungsi global Number() mengonversi tipe data string menjadi angka. Data yang diberikan pada parameter harus berupa karakter angka saja, dengan titik (separator) bila angka adalah bilangan desimal. Bila parameter berisi karakter selain angka dan/atau titik, Number() akan mengembalikan NaN (Not a Number).

var number1 = Number('90'); //number1 = 90
var number2 = Number('1.23'); //number2 = 1.23
var number3 = Number('4 5'); // number3 = NaN

//parseInt([String]) dan parseFloat([String])
